import {Injectable, UnauthorizedException} from '@nestjs/common';
import {InjectRepository} from '@nestjs/typeorm';
import {Column, getConnection, Repository} from 'typeorm';
import {User} from '../entitites/user.entity';
import {JwtService} from '@nestjs/jwt';
import {RegistrationDto} from "./dto/registration.dto";
import {LoginDto} from "./dto/login.dto";
import {Transaction} from "../entitites/transaction.entity";
import {Investment} from "../entitites/investment.entity";
import {InvestmentRepository} from "../investment/investment.repository";
import {BalanceRepository} from "../balance/balance.repository";
import {Balance} from "../entitites/balance.entity";
import * as moment from 'moment'

@Injectable()
export class AuthService {
    constructor(
        @InjectRepository(InvestmentRepository) private readonly investmentRepository: InvestmentRepository,
        @InjectRepository(BalanceRepository) private readonly balanceRepository: BalanceRepository,
        private jwtService: JwtService
    ) {
    }

    async registration(registrationDto: RegistrationDto) {
        const {login, password, email} = registrationDto;
        await getConnection()
            .createQueryBuilder()
            .insert()
            .into(User)
            .values([
                {login, password, email},
            ])
            .execute();

        const user = await getConnection()
            .getRepository(User)
            .createQueryBuilder("user")
            .where(
                "user.login = :login", {login: login}
            )
            .getOne();

        const payload = {username: login, sub: user.id};

        return {
            token: this.jwtService.sign(payload),
        };
    }

    async login(loginDto: LoginDto) {
        const {login, password} = loginDto;

        const user = await getConnection()
            .getRepository(User)
            .createQueryBuilder("user")
            .where(
                "user.login = :login", {login: login}
            )
            .getOne();

        if (!user || user.password !== password) {
            throw new UnauthorizedException();
        }

        const payload = {username: login, sub: user.id};

        return {
            token: this.jwtService.sign(payload),
        };
    }

    async transactions(userId: number) {
        const balances = await getConnection()
            .getRepository(Balance)
            .createQueryBuilder("balance")
            .where(
                "balance.user_id = :user_id", {user_id: userId}
            )
            .orderBy('id', 'ASC')
            .getMany();
        console.log('balances ', balances);
        return balances;
    }

    async investments() {
        return await getConnection()
            .getRepository(Investment)
            .createQueryBuilder("investment")
            .getMany();
    }

    async begin() {
        const createdDt = moment();
        console.log('createdDt ', createdDt);
        const prevInvestment = await getConnection()
            .getRepository(Investment)
            .createQueryBuilder("investment")
            .orderBy('id', 'DESC')
            .limit(1)
            .getOne();

        const beforeLastInvestment = await getConnection()
            .getRepository(Investment)
            .createQueryBuilder("investment")
            .orderBy('id', 'DESC')
            .limit(1)
            .getMany();

        const investment = await this.investmentRepository.createInvestment({
            created_dt: createdDt,
            tx_amt: 1,
            tx_dt: '1',
            net_amt: 1,
            tx_returned_amt: 1,
        });

        await this.balanceRepository.beginCycleUpdateBalances(investment, prevInvestment, beforeLastInvestment[1]);
    }

    async end() {
        const investment = await this.investmentRepository.updateLastInvestment({
            //tx_returned_amt: 2,
        });

        await this.balanceRepository.endCycleUpdateBalances(investment);
    }
}