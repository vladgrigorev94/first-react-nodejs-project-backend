import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Investment {
    @PrimaryGeneratedColumn()
    id: number;

    //unixtime — время создания
    @Column({nullable: true, type: "timestamp"})
    created_dt: string;

    //сумма инвестирования Investment
    @Column({nullable: true, type: "float"})
    tx_amt: number;

    //unixtime - время создания
    @Column()
    tx_dt: string;

    //сумма проекта Net stakes
    @Column({nullable: true, type: "float"})
    net_amt: number;

    //сумма возврата Return Tx
    @Column({nullable: true, type: "float"})
    tx_returned_amt: number;
}