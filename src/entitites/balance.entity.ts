import {Entity, Column, PrimaryGeneratedColumn} from 'typeorm';

@Entity()
export class Balance {
    @PrimaryGeneratedColumn()
    id: number;

    //пользователь
    @Column()
    user_id: number;

    @Column()
    investment_id: number;

    //берется из позапрошлого цикла
    @Column({nullable: true, type: "float"})
    incoming_reward: number;

    //просто сумма транзакций ушло-пришло от пользователя за период между циклами
    @Column({nullable: true, type: "float"})
    incoming_tx: number;

    @Column({nullable: true, type: "float"})
    balance_amt: number;

    @Column({nullable: true, type: "float"})
    cycle_in: number;

    //номер транзакции на выручку
    @Column({nullable: true, type: "float"})
    reward_id: number;

    //выручка
    @Column({nullable: true, type: "float"})
    reward_amt: number;

    @Column({nullable: true, type: "float"})
    cycle_out: number;

    @Column({nullable: true, type: "float"})
    total: number;
}