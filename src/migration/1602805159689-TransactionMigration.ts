import {MigrationInterface, QueryRunner} from "typeorm";

export class TransactionMigration1602805159689 implements MigrationInterface {
    name = 'TransactionMigration1602805159689'

    public async up(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`CREATE TABLE "investment" ("id" SERIAL NOT NULL, "created_dt" character varying NOT NULL, "tx_amt" integer NOT NULL, "tx_dt" character varying NOT NULL, "net_amt" integer NOT NULL, "tx_returned_amt" integer NOT NULL, CONSTRAINT "PK_ad085a94bd56e031136925f681b" PRIMARY KEY ("id"))`);
        await queryRunner.query(`CREATE TABLE "transaction" ("id" SERIAL NOT NULL, "created_dt" character varying NOT NULL, "user_id" integer NOT NULL, "tx_id" integer NOT NULL, "tx_amt" integer NOT NULL, "reward_id" integer NOT NULL, "reward_amt" integer NOT NULL, "balance_amt" integer NOT NULL, "total_amt" integer NOT NULL, CONSTRAINT "PK_89eadb93a89810556e1cbcd6ab9" PRIMARY KEY ("id"))`);
    }

    public async down(queryRunner: QueryRunner): Promise<void> {
        await queryRunner.query(`DROP TABLE "transaction"`);
        await queryRunner.query(`DROP TABLE "investment"`);
    }

}
