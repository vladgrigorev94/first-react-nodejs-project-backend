import {Investment} from '../entitites/investment.entity';
import {EntityRepository, Repository} from 'typeorm';

@EntityRepository(Investment)
export class InvestmentRepository extends Repository<Investment> {
    createInvestment = async (investment: {}) => {
        return await this.save(investment);
    };

    updateLastInvestment = async (data: {}) => {
        const lastInvestment = await this
            .createQueryBuilder('investment')
            .orderBy('id', 'DESC')
            .limit(1)
            .getOne();
        /*for (let index in data) {
            lastInvestment[index] = data[index];
        }*/

        return await this.save(lastInvestment);
    };
}